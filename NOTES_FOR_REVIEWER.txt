Installation
==============
 * Run 'bundle install' to install required gems (assuming you have installed 'Bundler' gem first).
 * It was written in Ruby v1.9.2.

Tests
==============
 * Run 'rspec spec' from project root dir to run all tests.
 * Integration test is at 'spec/reminder/reminder_spec.rb'.

Implementation
==============
 * It was not written with performance/scaling in mind.
 * Rules system is easily extendable.
 * Rspecs should be better organized.
 * Some parts of code are ugly.
 * Only 12 months of Ruby experience, so be kind!
