require 'spec_helper'
require 'date'

module Ringley
  module Rules

    describe QuarterlyChargeRule do
      describe "#process" do
        it "should return a valid due date" do
          start_date = "3rd Apr 2010"
          estate_1 = Estate.new("estate 1", "Quarterly", "1st Feb, 3rd May, 1st Aug, 5th Nov")
          estate_1.set_due_dates start_date

          res = QuarterlyChargeRule.new.process(estate_1, start_date)
          res.should eq Date.parse "3rd May 2010"
        end

        it "should not return a due date" do
          start_date = "4th May 2010"
          estate_1 = Estate.new("estate 1", "Quarterly", "1st Feb, 3rd May, 1st Aug, 5th Nov")
          estate_1.set_due_dates start_date

          res = QuarterlyChargeRule.new.process(estate_1, start_date)
          res.should eq nil
        end
      end
    end

    describe TwiceAYearRule do
      describe "#process" do
        it "should return a valid due date" do
          start_date = "28 Dec 2010"
          estate_1 = Estate.new("estate 1", "Twice a year ", "23rd Jan, 22nd Jun")
          estate_1.set_due_dates start_date

          res = QuarterlyChargeRule.new.process(estate_1, start_date)
          res.should eq Date.parse "23rd Jan 2011"
        end

        it "should not return a due date" do
          start_date = "24 Jan 2010"
          estate_1 = Estate.new("estate 1", "Twice a year ", "23rd Jan, 22nd Jun")
          estate_1.set_due_dates start_date

          res = QuarterlyChargeRule.new.process(estate_1, start_date)
          res.should eq nil
        end
      end
    end

  end
end
