require 'spec_helper'

module Ringley
  describe Reminder do

    describe "#on" do
      before(:each) do
        @estates = []
        @estates << Estate.new("0066S", "Quarterly", "1st Feb,  3rd May,  1st Aug,  5th Nov")
        @estates << Estate.new("0123S", "Quarterly", "28th Feb, 31st May, 31st Aug, 30th Nov")
        @estates << Estate.new("0250S", "Twice a year", "23rd Jan, 22nd Jun")

        @rules = []
        @rules << Ringley::Rules::QuarterlyChargeRule.new
        @rules << Ringley::Rules::TwiceAYearRule.new

        @reminder = Reminder.new(@rules)
      end

      it "should return correct due dates for 1st Jan 2009 " do
        result = @reminder.on("1st Jan 2009", @estates)
        result["0066S"].should eq Date.parse "1st Feb 2009"
        result["0250S"].should eq Date.parse "23rd Jan 2009"
      end

      it "should return correct due dates for 1st Feb 2009" do
        result = @reminder.on("1st Feb 2009", @estates)
        result["0066S"].should eq Date.parse "1st Feb 2009"
        result["0123S"].should eq Date.parse "28th Feb 2009"
      end

      it "should return correct due dates for date 2nd Feb 1979" do
        result = @reminder.on("2nd Feb 1979", @estates)
        result["0123S"].should eq Date.parse "28th Feb 1979"
      end

      it "should not return any due dates for date 15th Mar 1999", :focus => true do
        result = @reminder.on("15th Mar 1999", @estates)
        puts result if !result.keys.empty?
        result.keys.should be_empty
      end

      it "should return correct due dates for date 21st Apr 2013" do
        result = @reminder.on("21st Apr 2013", @estates)
        result["0066S"].should eq Date.parse "3rd May 2013"
      end

      it "should return correct due dates for date 22nd Apr 2017" do
        result = @reminder.on("22nd Apr 2017", @estates)
        result["0066S"].should eq Date.parse "3rd May 2017"
        result["0250S"].should eq Date.parse "22nd Jun 2017"
      end

      it "should return correct due dates for date 29th Apr 2000" do
        result = @reminder.on("29th Apr 2000", @estates)
        result["0066S"].should eq Date.parse "3rd May 2000"
        result["0250S"].should eq Date.parse "22nd Jun 2000"
      end

      it "should return correct due dates for date 30th Apr 2002" do
        result = @reminder.on("30th Apr 2002", @estates)
        result["0066S"].should eq Date.parse "3rd May 2002"
        result["0123S"].should eq Date.parse "31st May 2002"
        result["0250S"].should eq Date.parse "22nd Jun 2002"
      end

      it "should return correct due dates for date 29th Oct 2011" do
        result = @reminder.on("29th Oct 2011", @estates)
        result["0066S"].should eq Date.parse "5th Nov 2011"
      end

      it "should return correct due dates for date 30th Oct 2011" do
        result = @reminder.on("30th Oct 2011", @estates)
        result["0066S"].should eq Date.parse "5th Nov 2011"
        result["0123S"].should eq Date.parse "30th Nov 2011"
      end

      it "should return correct due dates for date 24th Dec 2006" do
        result = @reminder.on("24th Dec 2006", @estates)
        result["0250S"].should eq Date.parse "23rd Jan 2007"
      end

     end

 end


end
