require 'spec_helper'
require 'date'

module Ringley
  describe Estate do
   
    describe "#set_due_dates" do
      it "should create a sorted list of due dates when start date is 10 Dec 2019" do
        estate = Estate.new("estate_code", "quarterly", "1st Dec, 15 Dec, 21 Sep, 5th Jun")
        estate.set_due_dates "10 Dec 2019"
        estate.due_dates[0].should eq Date.parse "15 Dec 2019"
        estate.due_dates[1].should eq Date.parse "5th June 2020"
        estate.due_dates[2].should eq Date.parse "21 Sep 2020"
        estate.due_dates[3].should eq Date.parse "1 Dec 2020"
      end

      it "should create a sorted list of due dates when start date is 22 Sep 2019" do
        estate = Estate.new("estate_code", "quarterly", " 20th Nov, 1st Dec, 15 Dec, 21 Sep, 5th Jun")
        estate.set_due_dates "22 Sep 2019"
        estate.due_dates[0].should eq Date.parse "20th Nov 2019"
        estate.due_dates[1].should eq Date.parse "1st  Dec 2019"
        estate.due_dates[2].should eq Date.parse "15th Dec 2019"
        estate.due_dates[3].should eq Date.parse "5th  Jun 2020"
        estate.due_dates[4].should eq Date.parse "21 Sep 2020"
      end

    end

  end

end

