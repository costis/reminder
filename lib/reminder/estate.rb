require 'date'

module Ringley
  class Estate
    attr_reader :due_dates, :charge_period, :code

    def initialize(code, charge_period, dates)
      @code = code
      @charge_period = charge_period
      @dates_str = dates
    end

    # Create a sequence of due dates, all after start_date.
    # dates_str holds the pattern we use to build the sequence of due dates
    def set_due_dates start_date
      start_date = Date.parse start_date
      @due_dates = @dates_str.split(',').map{ |d| Date.parse d }.sort.collect do |date|
        if date.month < start_date.month or (date.month == start_date.month and date.day < start_date.day)
          Date.strptime("#{date.day}/#{date.month}/#{(start_date >> 12).year}", "%d/%m/%Y")
        else
          Date.strptime("#{date.day}/#{date.month}/#{start_date.year}", "%d/%m/%Y")
        end
      end.sort
    end

  end
end
