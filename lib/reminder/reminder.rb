module Ringley
  class Reminder

    def initialize(rules)
      @rules = rules 
    end

    def on(start_date, estates)
      result = {}
      estates.each do |estate|
        estate.set_due_dates(start_date)
        found_date = @rules.find { |r| r.name == estate.charge_period }.process(estate, start_date)
        result[estate.code] = found_date if found_date
      end
      return result
    end

  end
end
