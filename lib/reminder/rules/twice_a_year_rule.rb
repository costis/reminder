require 'date'

module Ringley
  module Rules
    class TwiceAYearRule < RuleAbstract

      def initialize
        @name = "Twice a year"
      end

      def process(estate, start_date)
        start_date = Date.parse start_date
        estate.due_dates.find do |d|
          r = Range.new(d << 2, d)
          r.include? start_date 
        end
      end
    end
  end
end
